public class helloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World");

        int number = 5;
        char letter = 'b';
        String age = "41";
        double decimal = number;
        float fraction = number;
        int realAge = Integer.parseInt(age);

        System.out.println("number: " + number);
        System.out.println("decimal: " + decimal);
        System.out.println("fraction: " + fraction);
        System.out.println("letter: " + letter);
        System.out.println("realAge: " + realAge);
        System.out.println("age: " + age);
    }
}
